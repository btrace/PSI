# 关于PSI

PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。

# PSI演示

PSI的演示见：<a target="_blank" href="https://psi.butterfly.mopaasapp.com/">https://psi.butterfly.mopaasapp.com/</a>

PC端请用`360浏览器`或者是`谷歌浏览器`访问
 
移动端扫码访问![移动端扫码访问](PSI_Mobile_URL.png)

# PSI的开源协议

PSI的开源协议为GPL v3

# PSI相关项目

1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help

2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 技术支持

如有技术方面的问题，可以提出Issue一起讨论：https://gitee.com/crm8000/PSI/issues

# 本地开发环境
参见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/06%20%E6%9C%AC%E5%9C%B0%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83">本地开发环境</a>

# 商务合作

QQ: crm8000@qq.com

PSI商业计划见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/00%20%E5%95%86%E4%B8%9A%E8%AE%A1%E5%88%92">PSI 商业计划</a>

**PSI官方不提供二次开发服务**

如果您有二次开发需求怎么解决？

**方式一**：到OSChina的众包平台：https://zb.oschina.net/ 去发布二次开发需求，让其他人接单，我们以微信和电话方式提供免费的技术支持。

这样做的好处是：

1. 您的二次开发需求以合理的成本解决
2. 让更多的程序员通过PSI赚到钱
3. PSI品牌价值进一步提升

**方式二**：直接联系PSI官方认证的二次开发服务商。

1. 云拓科技(大连)有限公司  http://www.yuntuoit.com 尧总 18698629583(微信同步)

**请在满足下列二次开发的基本要求后再与服务商联系，以免浪费彼此时间**
1. 二次开发预算不低于20万
2. 需求文档准备完毕


> 同时也招募其他城市的二次开发服务商加入，基本条件
> 1. 入门门槛，会员费：1万元/年
> 2. 二次开发收入全部归二次开发服务商(客户直接和二次服务商签订合同和支付款项)
> 3. 如果需PSI官方提供技术咨询等服务，费用另外商议

**PSI官方给二次开发服务商提供下列有偿服务**

1. 行业ERP产品经理服务：行业ERP产品的策划和设计
2. PSI技术设计的深度解析：技术培训
3. 研发管理指导：PSI实施方法论的具体讲解
4. 法律服务：PSI二开合同文本、合同审查、专利申请、软件著作权申请

> 说明：上面这些服务，也通过issue( https://gitee.com/crm8000/PSI/issues )的方式向所有人**免费**开放，只是通过issue的方式，只保证回复的准确性，不保证回复的及时性。
